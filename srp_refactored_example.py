# Single responsibility principle


def count_element(element, content):
    occurrences = 0
    element = element.lower()
    for entry in content:
        if entry.lower() == element:
            occurrences += 1
    return occurrences


def get_words(path):
    with open(path, 'r') as file:
        return file.read()


def percentage_of_word(search_phrase, words):
    search_phrase = search_phrase.lower()
    word_count = len(words)
    return count_element(search_phrase, words) / word_count


def main():
    content = get_words("pfad")
    words = content.split()
    percentage_of_word("hallo", words)


if __name__ == '__main__':
    main()
