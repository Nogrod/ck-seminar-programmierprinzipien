# Single level of abstraction

import os.path
import sys


def _parse_arguments():
    try:
        return sys.argv[1]
    except IndexError:
        raise ValueError


def _is_valid_file(path):
    if not os.path.isfile(path):
        raise ValueError


def _print_file(path):
    with open(path, "r") as file:
        for line in file:
            print(line)


def cat():
    path = _parse_arguments()
    _is_valid_file(path)
    _print_file(path)


if __name__ == '__main__':
    cat()
