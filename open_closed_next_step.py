# Open closed principle

def is_spam(comment):
    pass


def is_empty(comment):
    pass


def contains_harassment(comment):
    pass


def is_not_unique(comment):
    pass


def fancy_ai_comment_check(comment):
    pass


def is_comment_valid_new(comment):
    if is_spam(comment):
        return False
    if is_empty(comment):
        return False
    if contains_harassment(comment):
        return False
    if is_not_unique(comment):
        return False
    if fancy_ai_comment_check(comment):  # <-- KI Überprüfung ist eine änderung am Code
        return False
    return True
