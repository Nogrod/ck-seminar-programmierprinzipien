# Open closed principle

if export_table.partial_export.default is not None:
    (
        drop_table,
        lower_limit,
        source_filter_column,
        lower_limit,
        upper_limit,
    ) = _get_partial_export_parameters_for_default_config(export_table)
elif export_table.partial_export.daily_update is not None:
    (
        drop_table,
        lower_limit,
        source_filter_column,
        lower_limit,
        upper_limit,
    ) = _get_partial_export_parameters_for_daily_update_config(export_table)
elif export_table.partial_export.monthly_update is not None:
    (
        drop_table,
        source_filter_column,
        lower_limit,
        upper_limit,
    ) = _get_partial_export_parameters_for_monthly_update_config(export_table)
elif export_table.partial_export.weekly_update is not None:
    (
        drop_table,
        source_filter_column,
        lower_limit,
        upper_limit,
    ) = _get_partial_export_parameters_for_weekly_update_config(export_table)
elif export_table.partial_export.yearly_update is not None:
    (
        drop_table,
        source_filter_column,
        lower_limit,
        upper_limit,
    ) = _get_partial_export_parameters_for_yearly_update_config(export_table)


# ======================================================================================================================


intercal_update_functions = {
    export_table.partial_export.default: _get_partial_export_parameters_for_default_config,
    export_table.partial_export.daily_update: _get_partial_export_parameters_for_daily_update_config,
    export_table.partial_export.monthly_update: _get_partial_export_parameters_for_monthly_update_config,
    export_table.partial_export.weekly_update: _get_partial_export_parameters_for_weekly_update_config,
    export_table.partial_export.yearly_update: _get_partial_export_parameters_for_yearly_update_config,
}

for config_type, function in intercal_update_functions.items():
    if config_type is not None:
        (
            drop_table,
            source_filter_column,
            lower_limit,
            upper_limit,
        ) = function(export_table)
        break
else:  # Wenn kein breack in der foor loop vorgekommen ist kommt man in den else teil
    raise NotImplementedError(
            "For partial export use one of the following configs: "
            "DefaultPartialExportConfig, "
            "DailyUpdatePartialExportConfig, "
            "WeeklyUpdatePartialExportConfig, "
            "MonthlyUpdatePartialExportConfig, "
            "YearlyUpdatePartialExportConfig"
        )
