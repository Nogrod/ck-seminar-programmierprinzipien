# Open closed principle

def is_spam(comment):
    pass


def is_empty(comment):
    pass


def contains_harassment(comment):
    pass


def is_not_unique(comment):
    pass


def fancy_ai_comment_check(comment):
    pass


COMMENT_CHECKS = [is_spam, is_empty, contains_harassment, is_empty, fancy_ai_comment_check]


def is_comment_valid_new(comment):
    for is_valid in COMMENT_CHECKS:
        if not is_valid(comment):
            return False
    return True

