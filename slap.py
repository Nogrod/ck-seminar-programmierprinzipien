# Single level of abstraction

import os.path
import sys


def cat():
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError

    if not os.path.isfile(path):
        raise ValueError

    with open(path, "r") as file:
        for line in file:
            print(line)


if __name__ == '__main__':
    cat()
