# Single responsibility principle

def percentage_of_word(search_phrase, path):
    search_phrase = search_phrase.lower()
    with open(path, 'r') as file:
        content = file.read()
    words = content.split()
    number_of_words = len(words)
    occurrences = 0
    for word in words:
        if word.lower() == search_phrase:
            occurrences += 1
    return occurrences/number_of_words


if __name__ == '__main__':
    percentage_of_word("hallo", "path")
